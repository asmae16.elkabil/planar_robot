#include "trajectory_generation.h"

Polynomial::Polynomial(){};

Polynomial::Polynomial(const double &piIn, const double &pfIn, const double & DtIn){
  //TODO initialize the object polynomial coefficients
};

void          Polynomial::update(const double &piIn, const double &pfIn, const double & DtIn){
  //TODO update polynomial coefficients
};

const double  Polynomial::p     (const double &t){
  //TODO compute position
  return 0;
};

const double  Polynomial::dp    (const double &t){
  //TODO compute velocity
  return 0;
};

Point2Point::Point2Point(const Eigen::Vector2d & xi, const Eigen::Vector2d & xf, const double & DtIn){
  //TODO initialize object and polynomials
}

Eigen::Vector2d Point2Point::X(const double & time){
  //TODO compute cartesian position
  return Eigen::Vector2d::Zero();
}

Eigen::Vector2d Point2Point::dX(const double & time){
  //TODO compute cartesian velocity
  return Eigen::Vector2d::Zero();
}